package inteview.vending.service;

import inteview.vending.domain.Item;
import inteview.vending.domain.Money;
import inteview.vending.domain.MoneyManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.oxo42.stateless4j.StateMachine;
import com.github.oxo42.stateless4j.delegates.Action;

/**
 * Represents a Vending Machine.
 * @author bhatiaravi
 *
 */
public class Machine {
	
	private static final Logger logger = LogManager.getLogger(Machine.class);
	
	protected MoneyManager moneyManager;
	protected StateMachine<State, Trigger> vending = new StateMachine<State, Trigger>(State.AcceptMoney);
		
	public enum State { AcceptMoney, AddNickle, AddDime, AddQuarter, AddDollar, Vend_A, Vend_B, Vend_C, ReturnMoney};
	public enum Trigger { AcceptMoney, InsertNickle, InsertQuarter, InsertDime, InsertDollar, Get_A, Get_B, Get_C, ReturnMoney };
	public enum Command {Q, N, D, DOLLAR, Get_A, Get_B, Get_C, Coin_Return};
	
	public Machine() {
		
		moneyManager = new MoneyManager();
		initialize();
	}
	
	protected void initialize() {
		
		vending.configure(State.AcceptMoney)
			.permit(Trigger.InsertNickle, State.AddNickle)
			.permit(Trigger.InsertDime, State.AddDime)
			.permit(Trigger.InsertQuarter, State.AddQuarter)
			.permit(Trigger.InsertDollar, State.AddDollar)
			.permit(Trigger.ReturnMoney, State.ReturnMoney)
			.permit(Trigger.Get_A, State.Vend_A)
			.permit(Trigger.Get_B, State.Vend_B)
			.permit(Trigger.Get_C, State.Vend_C);
		
		
		vending.configure(State.AddNickle).onEntry(new Action() {

			@Override
			public void doIt() {
				addMoney(Money.NICKLE);				
			}
			
		})
			.permit(Trigger.AcceptMoney, State.AcceptMoney)
			.permit(Trigger.Get_A, State.Vend_A)
			.permit(Trigger.Get_B, State.Vend_B)
			.permit(Trigger.Get_C, State.Vend_C);
		
		vending.configure(State.AddDime).onEntry(new Action() {

			@Override
			public void doIt() {
				addMoney(Money.DIME);;				
			}
			
		})
			.permit(Trigger.AcceptMoney, State.AcceptMoney)
			.permit(Trigger.Get_A, State.Vend_A)
			.permit(Trigger.Get_B, State.Vend_B)
			.permit(Trigger.Get_C, State.Vend_C);
		
		vending.configure(State.AddQuarter).onEntry(new Action() {

			@Override
			public void doIt() {
				addMoney(Money.QUARTER);				
			}
			
		})
			.permit(Trigger.AcceptMoney, State.AcceptMoney)
			.permit(Trigger.Get_A, State.Vend_A)
			.permit(Trigger.Get_B, State.Vend_B)
			.permit(Trigger.Get_C, State.Vend_C);
		
		vending.configure(State.AddDollar).onEntry(new Action() {

			@Override
			public void doIt() {
				addMoney(Money.DOLLAR);			
			}
			
		})
			.permit(Trigger.AcceptMoney, State.AcceptMoney)
			.permit(Trigger.Get_A, State.Vend_A)
			.permit(Trigger.Get_B, State.Vend_B)
			.permit(Trigger.Get_C, State.Vend_C);
		
		vending.configure(State.Vend_A).onEntry(new Action() {

			@Override
			public void doIt() {				
				vend(Item.A);				
			}
			
		})
			.permit(Trigger.AcceptMoney, State.AcceptMoney)
			.permit(Trigger.ReturnMoney, State.ReturnMoney);
		
		vending.configure(State.Vend_B).onEntry(new Action() {

			@Override
			public void doIt() {				
				vend(Item.B);				
			}
			
		})
			.permit(Trigger.AcceptMoney, State.AcceptMoney)
			.permit(Trigger.ReturnMoney, State.ReturnMoney);
		
		vending.configure(State.Vend_C).onEntry(new Action() {

			@Override
			public void doIt() {				
				vend(Item.C);				
			}
			
		})
			.permit(Trigger.AcceptMoney, State.AcceptMoney)
			.permit(Trigger.ReturnMoney, State.ReturnMoney);
		
		vending.configure(State.ReturnMoney).onEntry(new Action() {

			@Override
			public void doIt() {				
				coinReturn();					
			}
			
		}).permit(Trigger.AcceptMoney, State.AcceptMoney);
				
	}
	
	protected void addMoney(Money money) {
		
		moneyManager.add(money);
		vending.fire(Trigger.AcceptMoney);
	}
	
	protected void vend(Item item) {
				
		double deposit = moneyManager.getTotalMoney();
		double cost = item.value();
				
		System.out.format("Deposit: %.2f, Cost: %.2f %n", deposit, cost);
		
		if (deposit < cost) {
			System.out.println("Insufficient Funds");
			vending.fire(Trigger.AcceptMoney);
		}
		
		if (deposit == cost) {
			System.out.println(item.name());
			moneyManager.empty();
			vending.fire(Trigger.AcceptMoney);
		}
		
		if (deposit > cost) {
			System.out.println(item.name());
			
			double remaining = deposit - cost;
			System.out.format("Remaining: %.2f%n", remaining);
			moneyManager.empty();
			
			for (Money money : Money.values()) {
				
				int c = (int) (remaining / money.value());
				if (c > 0) {
					remaining = remaining - (money.value() * c);
					for (int i = 0; i < c; i++) {
						moneyManager.add(money);
					}
				}				
			}
			
			vending.fire(Trigger.ReturnMoney);			
		}
	}
	
	protected void coinReturn() {
		
		List<Money> list = moneyManager.get();
		for (int i = 0; i < list.size(); i++) {
			System.out.print(list.get(i));
			if (i < list.size()-1) {System.out.print(" ,");}
		}
		System.out.println();
		moneyManager.empty();
		
		vending.fire(Trigger.AcceptMoney);
	}
	
	public static void main(String[] args) throws IOException {
						
		BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.format("Available commands: %s%n",Arrays.asList(Command.values()));
		System.out.println("Ready to accept input:");
		
		Machine machine = new Machine();
						
	   	boolean acceptInput = true;
		while (acceptInput) {
			
			System.out.format("[Money: %.2f]>", machine.moneyManager.getTotalMoney());
			String input = bufferRead.readLine();
			logger.debug(input);
			if (StringUtils.isEmpty(input)) continue;
			
			String[] arguments = input.split("\\s*,\\s*");
			for (String argument : arguments) {
				
				Command command = null;
				
				try {
					command = Command.valueOf(argument);
				} catch (IllegalArgumentException iae) {
					System.err.format("Illegal Argument: %s%n", argument);
					continue;
				}
							
				switch (command) {
				
				case N :
					machine.vending.fire(Trigger.InsertNickle);
					break;
					
				case D:
					machine.vending.fire(Trigger.InsertDime);
					break;
				
				case Q :
					machine.vending.fire(Trigger.InsertQuarter);
					break;
					
				case DOLLAR :
					machine.vending.fire(Trigger.InsertDollar);
					break;
				
				case Coin_Return :
					machine.vending.fire(Trigger.ReturnMoney);
					break;
					
				case Get_A :
					machine.vending.fire(Trigger.Get_A);
					break;
				
				case Get_B :
					machine.vending.fire(Trigger.Get_B);
					break;
				
				case Get_C :
					machine.vending.fire(Trigger.Get_C);
					break;
				}
			}
		}
	}
}
