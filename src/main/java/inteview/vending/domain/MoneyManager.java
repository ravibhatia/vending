package inteview.vending.domain;



/**
 * Store for money.
 *
 */
public class MoneyManager extends Store<Money> {
	
	public double getTotalMoney() {
		
		double total = 0;
		
		for (Money money : get()) {

			total = total + money.value();			
		}

		return total;
	}
}
