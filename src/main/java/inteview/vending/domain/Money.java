package inteview.vending.domain;


/**
 * Accepted types of money.
 *
 */
public enum Money {
	
	DOLLAR(1), QUARTER(0.25), DIME(0.10), NICKLE(0.05) ;
    
	private double value;

    private Money(double value) {
    	this.value = value;
    }
    
    public double value() {
    	return value;
    }
}
