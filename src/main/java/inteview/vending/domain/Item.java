package inteview.vending.domain;


/**
 * Represents an item to vend.
 *
 */
public enum Item {
	
	A(0.65), B(1), C(1.50);
    
	private double value;

    private Item(double value) {
    	this.value = value;
    }
    
    public double value() {
    	return value;
    }
}
