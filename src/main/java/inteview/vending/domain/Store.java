package inteview.vending.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a general store.
 */
public class Store<O> {
	
	protected List<O> list;
	
	public Store() {
		list = new ArrayList<O>();
	}
	
	public void add(O object) {		
		list.add(object);
	}
	
	public List<O> get() {		
		return list;
	}
	
	public void empty() {		
		list.clear();		
	}
}
