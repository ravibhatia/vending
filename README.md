# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

The purpose of this repository is for an interview.
http://code.joejag.com/coding-dojo-vending-machine/

### How do I get set up? ###

1) Fetch code.
2) mvn clean compile package
3) java -jar vending-0.0.1-SNAPSHOT.one-jar.jar

OR

1) Fetch target/vending-0.0.1-SNAPSHOT.one-jar.jar
2) java -jar vending-0.0.1-SNAPSHOT.one-jar.jar

### DISCLAIMER ###
The purpose of this code is only for an interview assignment. Several assumptions have been considered, and the solution has been but in place to only satisfy the requirements of the problem.